//
//  YoutubeVC.swift
//  YogaEase
//
//  Created by Nick Joliya on 25/10/23.
//
import UIKit
import youtube_ios_player_helper

class YoutubeVC: UIViewController, YTPlayerViewDelegate {
    @IBOutlet weak var playerView: YTPlayerView!

    var youtubeId = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        playerView.delegate = self
        playerView.load(withVideoId: youtubeId)
    }
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.playVideo()
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
    }
    func playVideo() {
        playerView.playVideo()
    }

    func pauseVideo() {
        playerView.pauseVideo()
    }
    
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


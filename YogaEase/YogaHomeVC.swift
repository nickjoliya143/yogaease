//
//  YogaHomeVC.swift
//  YogaEase
//
//  Created by Nick Joliya on 24/10/23.
//

import UIKit

class YogaHomeVC: UIViewController {

    @IBOutlet weak var CV_Yoga: UICollectionView!
 
    var yogaData  = [YogaPose]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        yogaData = YogaPoseDataManager.shared.getYogaPoseList()
        CV_Yoga.delegate = self
        CV_Yoga.dataSource = self
        CV_Yoga.register(UINib(nibName: "CVC_Yoga", bundle: nil), forCellWithReuseIdentifier: "CVC_Yoga")
       
    }
    
    @IBAction func btnMore(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension YogaHomeVC : UICollectionViewDataSource ,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return yogaData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVC_Yoga", for: indexPath) as! CVC_Yoga
       
        cell.lblTitle.text  = yogaData[indexPath.row].name
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width  , height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailedVC") as! DetailedVC
        
        vc.desc = yogaData[indexPath.row].longDescription
        vc.titlestring = yogaData[indexPath.row].name
        vc.imageString = yogaData[indexPath.row].image
        vc.youtubeId = yogaData[indexPath.row].shortDescription
        
        self.navigationController?.pushViewController(vc, animated:true)
    }
}

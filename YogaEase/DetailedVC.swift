//
//  DetailedVC.swift
//  YogaEase
//
//  Created by Nick Joliya on 24/10/23.
//

import UIKit

class DetailedVC: UIViewController {

    @IBOutlet weak var txtvDescription: UITextView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBG: UIImageView!
    
    var titlestring = ""
    var desc = ""
    var imageString = ""
    var youtubeId = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = titlestring
        txtvDescription.text = desc
        imgBG.image = UIImage(named: imageString)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnPlay(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "YoutubeVC") as! YoutubeVC
        vc.youtubeId = youtubeId
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

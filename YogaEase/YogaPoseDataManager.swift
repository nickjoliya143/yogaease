//
//  YogaPoseDataManager.swift
//  YogaEase
//
//  Created by Nick Joliya on 24/10/23.
//
import Foundation

struct YogaPose {
    let id: Int
    let name: String
    let shortDescription: String
    let longDescription: String
    let image: String

    init(id: Int, name: String, shortDescription: String, longDescription: String, image: String) {
        self.id = id
        self.name = name
        self.shortDescription = shortDescription
        self.longDescription = longDescription
        self.image = image
    }
}

class YogaPoseDataManager {
    static let shared = YogaPoseDataManager()

    private init() {
        // Private initializer to enforce singleton pattern
    }

    func getYogaPoseList() -> [YogaPose] {
        var poseList = [YogaPose]()

        let pose1 = YogaPose(
            id: 1,
            name: "Tadasana",
            shortDescription: "dccBihALY7Y",
            longDescription: "Tadasana, also known as Mountain Pose, is a basic standing yoga posture. It is the starting position for all the standing poses. The word 'Tada' means 'mountain' in Sanskrit. It is a simple and effective asana to improve posture, balance, and mental clarity.",
            image: "Tadasana"
        )

        let pose2 = YogaPose(
            id: 2,
            name: "Adho Mukha Svanasana",
            shortDescription: "EC7RGJ975iM",
            longDescription: "Adho Mukha Svanasana, commonly known as Downward-Facing Dog, is a fundamental yoga pose. It is an inverted pose that provides a deep stretch to the entire body. This pose is excellent for improving flexibility and strength.",
            image: "AdhoMukha"
        )

        let pose3 = YogaPose(
            id: 3,
            name: "Virabhadrasana",
            shortDescription: "Mn6RSIRCV3w",
            longDescription: "Virabhadrasana, or Warrior I, is a dynamic yoga pose that symbolizes the warrior's triumph. This asana strengthens the legs, arms, and back while enhancing concentration and balance.",
            image: "Virabhadrasana"
        )

        let pose4 = YogaPose(
            id: 4,
            name: "Sukhasana",
            shortDescription: "ri9B8IzLXIY",
            longDescription: "Sukhasana, also known as Easy Pose, is a simple seated yoga pose. It is an ideal posture for meditation and pranayama (breath control) exercises. Sukhasana promotes inner calm and relaxation.",
            image: "Sukhasana"
        )

        let pose5 = YogaPose(
            id: 5,
            name: "Bhujangasana",
            shortDescription: "99RR2vRvgi8",
            longDescription: "Bhujangasana, or Cobra Pose, is a rejuvenating backbend. It strengthens the spine and lower back muscles while opening the chest and lungs. This pose is excellent for alleviating back pain.",
            image: "Bhujangasana"
        )

        let pose6 = YogaPose(
            id: 6,
            name: "Balasana",
            shortDescription: "Ppku7i3ypGM",
            longDescription: "Balasana, or Child's Pose, is a resting yoga pose that provides deep relaxation and stress relief. It gently stretches the lower back and hips while calming the mind.",
            image: "Balasana"
        )

        let pose7 = YogaPose(
            id: 7,
            name: "Vrikshasana",
            shortDescription: "QeYhMXJWpJg",
            longDescription: "Vrikshasana, or Tree Pose, is a standing yoga pose that enhances balance and concentration. It involves placing one foot on the inner thigh of the other leg while keeping the spine straight.",
            image: "Vrikshasana"
        )

        let pose8 = YogaPose(
            id: 8,
            name: "Utkatasana",
            shortDescription: "4xyTmX_OMiM",
            longDescription: "Utkatasana, also known as Chair Pose, is a strengthening yoga pose. It engages the leg muscles and promotes endurance. This pose is beneficial for building core strength.",
            image: "Utkatasana"
        )

        let pose9 = YogaPose(
            id: 9,
            name: "Paschimottanasana",
            shortDescription: "T8sgVyF4Ux4",
            longDescription: "Paschimottanasana, or Seated Forward Bend, is a seated yoga pose that provides a deep stretch to the spine, hamstrings, and entire back. It is beneficial for calming the mind and relieving stress.",
            image: "Paschimottanasana"
        )

        let pose10 = YogaPose(
            id: 10,
            name: "Savasana",
            shortDescription: "SIqoCQ5dLlE",
            longDescription: "Savasana, or Corpse Pose, is the final relaxation pose in yoga. It involves lying flat on the back with complete stillness. Savasana promotes deep relaxation and rejuvenation.",
            image: "Savasana1"
        )
        let pose11 = YogaPose(
            id: 11,
            name: "Ardha Matsyendrasana",
            shortDescription: "51EqCa6ZGCw",
            longDescription: "Ardha Matsyendrasana, or Half Lord of the Fishes Pose, is a seated yoga twist. It promotes spinal flexibility and aids digestion. This pose is named after the legendary yogi Matsyendranath.",
            image: "ArdhaMatsyendrasana"
        )

        let pose12 = YogaPose(
            id: 12,
            name: "Ustrasana",
            shortDescription: "dhGeqzMzyhY",
            longDescription: "Ustrasana, or Camel Pose, is a backbending yoga pose. It stretches the entire front body and opens the heart and chest. This asana is known for its heart-opening benefits.",
            image: "Ustrasana"
        )

        let pose13 = YogaPose(
            id: 13,
            name: "Dhanurasana",
            shortDescription: "4P2mYcOGxbU",
            longDescription: "Dhanurasana, or Bow Pose, is a dynamic backbend. It involves bending the body back, resembling the shape of a bow. This asana provides a deep stretch to the entire front body.",
            image: "Dhanurasana"
        )

        let pose14 = YogaPose(
            id: 14,
            name: "Setu Bandhasana",
            shortDescription: "Exyjk7hNs4c",
            longDescription: "Setu Bandhasana, or Bridge Pose, is a backbending yoga pose. It strengthens the back and glutes while opening the chest. This asana is effective for countering the effects of sitting for extended periods.",
            image: "SetuBandhasana"
        )

        let pose15 = YogaPose(
            id: 15,
            name: "Marjaryasana",
            shortDescription: "LGLIyrfTiUc",
            longDescription: "Marjaryasana, or Cat-Cow Pose, is a gentle flow sequence that warms up the spine. It involves alternating between arching and rounding the back. This asana is often used for spinal flexibility and stress relief.",
            image: "Marjaryasana"
        )

        let pose16 = YogaPose(
            id: 16,
            name: "Navasana",
            shortDescription: "QVEINjrYUPU",
            longDescription: "Navasana, or Boat Pose, is a core-strengthening yoga pose. It engages the abdominal muscles and hip flexors. This asana is effective for building core strength and balance.",
            image: "Navasana"
        )

        let pose17 = YogaPose(
            id: 17,
            name: "Utthita Trikonasana",
            shortDescription: "FSdVBFpT6i4",
            longDescription: "Utthita Trikonasana, or Extended Triangle Pose, is a standing yoga pose that stretches and strengthens the legs and spine. It involves a deep stretch while keeping the body in a triangular shape.",
            image: "UtthitaTrikonasana"
        )

        let pose18 = YogaPose(
            id: 18,
            name: "Sirsasana",
            shortDescription: "VrenTA2IFjI",
            longDescription: "Sirsasana, or Headstand, is an advanced yoga inversion. It involves balancing the body on the head and forearms. This asana builds strength, balance, and mental focus.",
            image: "Sirsasana"
        )

        let pose19 = YogaPose(
            id: 19,
            name: "Ardha Chandrasana",
            shortDescription: "i6EPVHHlFNk",
            longDescription: "Ardha Chandrasana, or Half Moon Pose, is a balancing yoga pose. It requires balancing on one leg while extending the other leg and one hand toward the ground. This asana improves balance and strength.",
            image: "ArdhaChandrasana"
        )

        let pose20 = YogaPose(
            id: 20,
            name: "Garudasana",
            shortDescription: "FTWFM-lL5jQ",
            longDescription: "Garudasana, or Eagle Pose, is a balancing yoga pose that involves wrapping one leg around the other while twisting the arms. It stretches the shoulders and legs while improving balance and focus.",
            image: "Garudasana"
        )

        poseList.append(contentsOf: [pose1, pose2, pose3, pose4, pose5, pose6, pose7, pose8, pose9, pose10,pose11, pose12, pose13, pose14, pose15, pose16, pose17, pose18, pose19, pose20])

        return poseList
    }
}
